<?php 
	echo "Odd numbers 1 to 10:";
     for($counter = 1; $counter <= 10; $counter++) {  
        /* Even numbers are divisible by 2 */ 
        if($counter%2 == 1) { 
            /* counter is even, print it */
            echo " ". $counter; 
        }  
    }
 ?>