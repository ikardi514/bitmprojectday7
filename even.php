<?php 
	echo "Even numbers 1 to 10:";
     for($counter = 1; $counter <= 10; $counter++) {  
        /* Even numbers are divisible by 2 */ 
        if($counter%2 == 0) { 
            /* counter is even, print it */
            echo " ". $counter; 
        }  
    }
 ?>